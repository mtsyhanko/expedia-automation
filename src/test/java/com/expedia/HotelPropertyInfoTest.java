package com.expedia;

import com.expedia.exel.ExpediaExelHelper;
import com.expedia.ui.actions.NavigationAction;
import com.expedia.ui.actions.PropertyAmenitiesModalActions;
import com.expedia.ui.actions.PropertyInformationActions;
import com.expedia.ui.actions.RoomInformationActions;
import org.apache.commons.lang3.math.NumberUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;

public class HotelPropertyInfoTest extends BaseTest {

    private final NavigationAction navigationAction = new NavigationAction();
    private PropertyInformationActions propertyInformationActions;
    private RoomInformationActions roomInformationActions;
    private PropertyAmenitiesModalActions propertyAmenitiesModalActions;
    public static final String REPORT_PATH = "hotels/";
    public static final String PROPERTY_GALLERY_PATH = "/property_gallery";
    public static final String ROOMS_GALLERY_PATH = "/rooms_gallery/";
    public static final String XLS_EXTENSION = ".xls";
    private ExpediaExelHelper expediaExelHelper;
    private String hotelName;

    @BeforeMethod()
    public void setUp(Object[] params) {
        hotelName = parseHotelName(params[NumberUtils.INTEGER_ZERO].toString());
        expediaExelHelper = new ExpediaExelHelper(REPORT_PATH + hotelName, hotelName + XLS_EXTENSION);
        propertyInformationActions = new PropertyInformationActions(expediaExelHelper);
        propertyAmenitiesModalActions = new PropertyAmenitiesModalActions(expediaExelHelper);
        roomInformationActions = new RoomInformationActions(expediaExelHelper);
    }

    @AfterMethod
    public void saveInfoToXls() {
        expediaExelHelper.save();
    }

    @Test(description = "Load hotel info", dataProviderClass = HotelsProvider.class, dataProvider = "hotelLinks")
    public void verifyAbilityToFillContactUsForm(String hotelLink) {
        navigationAction.openHotelViewPage(hotelLink);
        propertyInformationActions.populateGeneralInfo();
        propertyAmenitiesModalActions.populateAmenitiesData();
        propertyInformationActions.loadImages(REPORT_PATH + hotelName + PROPERTY_GALLERY_PATH);
        var availableRooms = roomInformationActions.getAvailableRoomsList();

        availableRooms.forEach(room -> {
            roomInformationActions.openRoomModal(room);
            roomInformationActions.populateRoomData(room);
            roomInformationActions.loadRoomImages(room, REPORT_PATH + hotelName + ROOMS_GALLERY_PATH + room);
            roomInformationActions.closeRoomView();
        });
    }

    private String parseHotelName(String hotelLink) {
        return Arrays.asList(hotelLink.split("\\.")).get(NumberUtils.INTEGER_ZERO);
    }
}
