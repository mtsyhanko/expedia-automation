package com.expedia;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.expedia.core.EnvProperties;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;

public class BaseTest {

    @BeforeSuite(alwaysRun = true)
    public void setUpWebDriver() {
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        ChromeOptions chromeOptions = new ChromeOptions();
        if (EnvProperties.getEnvProperties().isHeadless()) {
            chromeOptions.addArguments("--headless");
        }

        Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        WebDriverManager.chromedriver().setup();
    }

    @AfterMethod(alwaysRun = true)
    public void quitDriver() {
        WebDriverRunner.getWebDriver().quit();
    }

}
