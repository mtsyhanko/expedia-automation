package com.expedia;

import org.testng.annotations.DataProvider;

public class HotelsProvider {

    @DataProvider(name = "hotelLinks")
    public Object[][] hotelLinks() {
        return new Object[][]{
                {"Acapulco-Hotels-Fiesta-Americana-Acapulco-Villas.h26560.Hotel-Information"},
                {"Acapulco-Hotels-Banyan-Tree-Cabo-Marques.h3412223.Hotel-Information"}
        };
    }
}
