package com.expedia.ui.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

public class MatcherUtils {

    public static String parseDigitWithFollowingText(String input, String followingText) {
        Pattern pattern = Pattern.compile(format("(\\d+) %s(?!%s)", followingText, followingText));
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return matcher.group(NumberUtils.INTEGER_ONE);
        }
        return StringUtils.EMPTY;
    }

    public static String parseDigit(String input) {
        Pattern pattern = Pattern.compile("(\\d+)");
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return matcher.group(NumberUtils.INTEGER_ONE);
        }
        return StringUtils.EMPTY;
    }

    public static boolean hasWords(String input, List<String>... lists) {
        for (List<String> words : lists) {
            if (words.stream().allMatch(word -> input.toLowerCase().contains(word.toLowerCase()))) {
                return true;
            }
        }
        return false;

    }

}
