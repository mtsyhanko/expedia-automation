package com.expedia.ui.utils;

import com.codeborne.selenide.Selenide;

import java.util.Random;

public class RandomWaitUtils {
    public static void randomWait() {
        Selenide.sleep(new Random().nextInt(200));
    }
}
