package com.expedia.ui.pages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;

@Getter
public class RoomInfoModal {
    private SelenideElement modalHeader = $x("//*[contains(@id,'app-layer-room')]//h2[contains(text(),'Room information')]");
    private SelenideElement roomTypeLabel = $x("//*[contains(@id,'app-layer-room')]//h3");
    private SelenideElement imageButton = $(".uitk-gallery-carousel-items img");
    private SelenideElement buttonPrev = $x("//button[contains(@class,'allery-carousel-button-prev')]");
    private SelenideElement buttonNext = $x("//button[contains(@class,'allery-carousel-button-next')]");
    private SelenideElement closeButton = $x("//button[@data-icon='tool-close']");

    public SelenideElement getRoomInfoItemByText(String roomItemText) {
        return $x(format("//*[contains(@id,'app-layer-room-info')]//*[contains(text(),\"%s\")]", roomItemText));
    }
}
