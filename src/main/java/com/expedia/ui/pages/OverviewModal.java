package com.expedia.ui.pages;

import com.codeborne.selenide.ElementsCollection;
import lombok.Data;

import static com.codeborne.selenide.Selenide.$$x;

@Data
public class OverviewModal {
    private ElementsCollection restaurantsList =
            $$x("//*[contains(text(),'Restaurants')]/parent::*[contains(@class, 'flex-item')]//li");

}
