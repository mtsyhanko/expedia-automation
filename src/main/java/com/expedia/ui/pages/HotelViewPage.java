package com.expedia.ui.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import javax.swing.text.AttributeSet;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;
import static java.lang.String.format;

@Getter
public class HotelViewPage {
    private SelenideElement hotelNameLabel = $x("//*[@data-stid='content-hotel-title']//h1");
    private SelenideElement seeAllPopularAmenities = $x("//*[@data-stid='hotel-amenities-list']//button");
    private SelenideElement seeAllSafetyPractices = $x("//*[contains(text(),'Cleaning and safety practices')]/following::button");
    private ElementsCollection galleryButtons = $$("button.uitk-image-link");
    private SelenideElement roomsTab = $x("//*[@href='#Offers']");
    private ElementsCollection roomNames = $$x("//*[contains(@data-stid,'property-offer')]//h3");
    private SelenideElement businessServicesLabel = $x("//*[@itemprop='amenityFeature']//*[contains(text(),'Business services')]");
    private SelenideElement propertyDescriptionSection = $x("//*[contains(text(),'About this property')]"
            + "/ancestor::section//*[@data-stid='content-sub-section']");
    private SelenideElement address1MetaElement = $x("//*[@itemprop='streetAddress']");
    private SelenideElement addressCountryCode = $x("//*[@itemprop='addressCountry']");
    private SelenideElement addressRegionCode = $x("(//*[@class='Breadcrumbs']//a)[4]");
    private SelenideElement addressCity = $x("//*[@data-stid='content-hotel-address']/following::*[@itemprop='name']");
    private SelenideElement airportLabel = $x("//*[contains(text(),'Getting around')]/parent::*[contains(@class,'flex-item')]//li");

    public SelenideElement getMoreDetailsButtonByName(String room) {
        return $x(format("//*[contains(text(),'%s')]//following::*[contains(text(),'More details')]", room));
    }

    public List<String> getImportantInfoSentences() {
        return Arrays.asList($x("//*[contains(text(),'Important information')]/ancestor::section").text().split("\n"));
    }

    public List<String> getCleaningAndSafetyPractices() {
        return Arrays.asList($("#cleaningAndSafetyPractices").text().split("\n"));
    }

    public SelenideElement getPoliciesElement(String room) {
        return $x(format("//*[contains(text(),'%s')]//following::*[contains(text(),'More details')]", room));
    }

    public SelenideElement getElementByText(String text) {
        return $x(format("//*[contains(text(),'%s')]", text));
    }
}
