package com.expedia.ui.pages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

@Getter
public class GalleryViewModal {
    private SelenideElement buttonPrev = $x("//button[contains(@class,'gallery-controls-prev')]");
    private SelenideElement buttonNext = $x("//button[contains(@class,'gallery-controls-next')]");
    private SelenideElement closeButton = $x("//button[@data-icon='tool-close']");
    private SelenideElement imageCounter = $x("//*[@data-stid='media-gallery-image-counter']");
    private SelenideElement image = $("#app-layer-media-gallery .uitk-media-gallery-image-active img");
}
