package com.expedia.ui.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;

@Getter
public class PropertyAmenitiesModal {
    private SelenideElement closeModalViewButton = $x("//*[@data-icon='tool-close']");
    private SelenideElement familyFriendlySection = $x("//*[contains(text(),'family friendly')]");
    private SelenideElement frontDeskLabel = $x("//li[text()='24/7 front desk']");

    //Guest Services
    private SelenideElement guestServicesFontDeskLabel
            = $x("//*[@id='Amenities']//*[text()='24-hour front desk']");

    private SelenideElement conciergeServicesLabel
            = $x("//*[@id='Amenities']//*[text()='Concierge services']");

    public SelenideElement getPropByText(String amenityName) {
        return $x(format("//*[@id='Amenities']//*[contains(text(),\"%s\")]", amenityName));
    }

    public SelenideElement getPropertyByTexts(String... texts) {
        if (texts.length == 1) {
            return $x(format("//*[@id='Amenities']//*[contains(text(),\"%s\")]",
                    texts[NumberUtils.INTEGER_ZERO]));
        }
        StringBuilder sb = new StringBuilder(format("//*[contains(text(),\"%s\")", texts[NumberUtils.INTEGER_ZERO]));
        for (int i = 1; i < texts.length; i++) {
            sb.append(format(" and contains(text(), \"%s\")", texts[i]));
        }
        sb.append("]");
        return $x(sb.toString());
    }

    public List<String> getAmenitiesItemsTexts() {
        return Arrays.asList($x("//*[@id='Amenities']").text().split("\n"));
    }
}
