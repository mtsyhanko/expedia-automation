package com.expedia.ui.pages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import java.io.File;

import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;

@Getter
public class PoliciesModal {
    private SelenideElement checkInInfoLabel = $x("//h3[contains(text(), 'Check-in')]/ancestor::*[@data-stid='content-item']"
            + "//*[contains(@class,'uitk-text-default-theme')]");
    private SelenideElement checkOutInfoLabel = $x("//h3[contains(text(), 'Check-out')]/ancestor::*[@data-stid='content-item']"
            + "//*[contains(@class,'uitk-text-default-theme')]");

    public SelenideElement getPolicyByText(String str) {
        return $x(format("//*[@id='Policies']//*[contains(text(),'%s')]", str));
    }
}
