package com.expedia.ui.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemModel {
    private String itemName;
    private String itemDescription;
    private boolean itemAvailability;
}
