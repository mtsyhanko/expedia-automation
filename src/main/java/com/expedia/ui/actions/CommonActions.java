package com.expedia.ui.actions;


import static org.assertj.core.api.Assertions.assertThat;

public class CommonActions {
    public void verifyBrowserUrlContainsPath(String path) {
        assertThat("DriverProvider.getDriver().getCurrentUrl()").contains(path);
    }
}
