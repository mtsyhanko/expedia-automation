package com.expedia.ui.actions;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.expedia.core.EnvProperties;
import io.qameta.allure.Step;

public class NavigationAction {
    public void navigateTo(String url) {
        WebDriverRunner.getWebDriver().navigate().to(url);
    }

    @Step("Open Home Page")
    public void openHotelViewPage(String hotelUrl) {
        Selenide.open(EnvProperties.getEnvProperties().getBaseUrl() + "/" + hotelUrl);
    }
}
