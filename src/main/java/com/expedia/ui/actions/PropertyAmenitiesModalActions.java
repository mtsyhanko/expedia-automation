package com.expedia.ui.actions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.expedia.exel.ExpediaExelHelper;
import com.expedia.ui.pages.PropertyAmenitiesModal;
import com.expedia.ui.utils.MatcherUtils;
import com.google.common.collect.ImmutableMap;
import io.qameta.allure.Step;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Callable;

import static com.expedia.ui.utils.MatcherUtils.*;

@Getter
public class PropertyAmenitiesModalActions {
    private final PropertyAmenitiesModal propertyAmenitiesModal = new PropertyAmenitiesModal();
    private ExpediaExelHelper expediaExelHelper;

    private Map<String, Callable<Boolean>> amenitiesAvailabilityMap = ImmutableMap.<String, Callable<Boolean>>builder()
            .put("Family friendly", () -> propertyAmenitiesModal.getFamilyFriendlySection().is(Condition.exist))
            //Guest services
            .put("24-hour front desk", () -> propertyAmenitiesModal.getPropByText("24-hour front desk").exists())
            .put("Tour/ticket assistance", () -> propertyAmenitiesModal.getPropByText("Tour/ticket assistance").exists())
            .put("Concierge services", () -> propertyAmenitiesModal.getPropByText("Concierge services").exists())
            .put("ATM/banking services", () -> propertyAmenitiesModal.getPropByText("ATM/banking services").exists())
            .put("Newspapers in lobby", () -> propertyAmenitiesModal.getPropByText("Newspapers in lobby").exists())
            .put("Luggage storage", () -> propertyAmenitiesModal.getPropByText("Luggage storage").exists())
            .put("Safety deposit box at front desk", () -> propertyAmenitiesModal.getPropByText("Safety deposit box at front desk").exists())
            .put("Currency exchange", () -> propertyAmenitiesModal.getPropByText("Currency exchange").exists())
            .put("Porter/bellhop", () -> propertyAmenitiesModal.getPropByText("Porter/bellhop").exists())
            .put("Laundry services", () -> propertyAmenitiesModal.getPropByText("Laundry facilities").exists())
            .put("Dry cleaning service", () -> propertyAmenitiesModal.getPropByText("Dry cleaning service").exists())
            .put("Laundry facilities", () -> propertyAmenitiesModal.getPropByText("Laundry facilities").exists())
            .put("Daily housekeeping", () -> propertyAmenitiesModal.getPropByText("Daily housekeeping").exists())
            .put("Ironing service", () -> propertyAmenitiesModal.getPropByText("Ironing service").exists())
            .put("Shops available on site", () -> propertyAmenitiesModal.getPropByText("Shops available on site").exists())
            .put("Gift shop", () -> propertyAmenitiesModal.getPropByText("Gift shop").exists())
            .put("Grocery/convenience store", () -> propertyAmenitiesModal.getPropByText("Grocery/convenience store").exists())
            .put("Grocery shopping service", () -> propertyAmenitiesModal.getPropByText("Grocery shopping service").exists())
            .put("Hair/Beauty salon", () -> propertyAmenitiesModal.getPropByText("Hair/Beauty salon").exists())
            .put("Television in common areas", () -> propertyAmenitiesModal.getPropByText("Television in common areas").exists())
            .put("Elevator", () -> propertyAmenitiesModal.getPropByText("Elevator").exists())
            .put("Computer stations", () -> propertyAmenitiesModal.getPropByText("Computer station").exists())
            .put("Multilingual staff", () -> propertyAmenitiesModal.getPropByText("Multilingual staff").exists())
            .put("Wedding services", () -> propertyAmenitiesModal.getPropByText("Wedding services").exists())
            .put("Turndown service", () -> propertyAmenitiesModal.getPropByText("Turndown service").exists())
            .put("VIP room facilities", () -> propertyAmenitiesModal.getPropByText("VIP room facilities").exists())
            //Languages spoken
            .put("English", () -> propertyAmenitiesModal.getPropByText("English").exists())
            .put("Spanish", () -> propertyAmenitiesModal.getPropByText("Spanish").exists())
            .put("French", () -> propertyAmenitiesModal.getPropByText("French").exists())
            .put("Japanese", () -> propertyAmenitiesModal.getPropByText("Japanese").exists())
            .put("Russian", () -> propertyAmenitiesModal.getPropByText("Russian").exists())
            // Spa
            .put("Spa", () -> propertyAmenitiesModal.getPropByText("Spa").exists())
            .put("Sauna", () -> propertyAmenitiesModal.getPropByText("Sauna").exists())
            .put("Spa lounge/relaxation area", () -> propertyAmenitiesModal.getPropByText("Spa lounge/relaxation area").exists())
            .put("Massages", () -> propertyAmenitiesModal.getPropByText("Massages").exists())
            .put("Beauty Services", () -> propertyAmenitiesModal.getPropByText("Beauty Services").exists())
            .put("Makeup services", () -> propertyAmenitiesModal.getPropByText("Makeup services").exists())
            .put("Waxing Services", () -> propertyAmenitiesModal.getPropByText("Waxing Services").exists())
            .put("Hair treatments", () -> propertyAmenitiesModal.getPropByText("Hair treatments").exists())
            .put("Aromatherapy", () -> propertyAmenitiesModal.getPropByText("Aromatherapy").exists())
            .put("Ayurvedic treatments", () -> propertyAmenitiesModal.getPropByText("Ayurvedic treatments").exists())
            .put("Body scrubs", () -> propertyAmenitiesModal.getPropByText("Body scrubs").exists())
            .put("Body treatments", () -> propertyAmenitiesModal.getPropByText("Body treatments").exists())
            .put("Body wraps", () -> propertyAmenitiesModal.getPropByText("Body wraps").exists())
            .put("Facial treatments", () -> propertyAmenitiesModal.getPropByText("Facial treatments").exists())
            .put("Manicures and pedicures", () -> propertyAmenitiesModal.getPropByText("Manicures and pedicures").exists())
            .put("Hydrotherapy treatments", () -> propertyAmenitiesModal.getPropByText("Hydrotherapy treatments").exists())
            .put("Reflexology", () -> propertyAmenitiesModal.getPropByText("Reflexology").exists())

            // Sports
            .put("Tennis on site", () -> propertyAmenitiesModal.getPropByText("Tennis on site").exists())
            .put("Tennis equipment", () -> propertyAmenitiesModal.getPropByText("Tennis equipment").exists())
            .put("Tennis lessons", () -> propertyAmenitiesModal.getPropByText("Tennis lessons").exists())
            .put("Volleyball", () -> propertyAmenitiesModal.getPropByText("Volleyball").exists())
            .put("Water activities", () -> propertyAmenitiesModal.getPropByText("Water activities").exists())
            .put("Fishing", () -> propertyAmenitiesModal.getPropByText("Fishing").exists())
            .put("Scuba diving", () -> propertyAmenitiesModal.getPropByText("Scuba diving").exists())
            .put("Snorkeling", () -> propertyAmenitiesModal.getPropByText("Snorkeling").exists())
            .put("Boat tours", () -> propertyAmenitiesModal.getPropByText("Boat tours").exists())
            .put("Marina", () -> propertyAmenitiesModal.getPropByText("Marina").exists())
            .put("Parasailing", () -> propertyAmenitiesModal.getPropByText("Parasailing").exists())
            .put("Surfing/boogie boarding", () -> propertyAmenitiesModal.getPropByText("Surfing/boogie boarding").exists())
            .put("Water sport facilities", () -> propertyAmenitiesModal.getPropByText("Water sport facilities").exists())

            //Accessibility
            .put("Facilities for disabled guests", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible").exists())
            .put("Wheelchair accessible (may have limitations)", () -> propertyAmenitiesModal.getPropByText("Wheelchair accessible (may have limitations)").exists())
            .put("Wheelchair-accessible registration desk / front desk",
                    () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible registration desk").exists()
                            || propertyAmenitiesModal.getPropByText("Wheelchair-accessible front desk").exists())
            .put("Wheelchair-accessible concierge desk", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible concierge desk").exists())
            .put("Wheelchair-accessible fitness center", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible fitness center").exists())
            .put("Wheelchair-accessible lounge", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible lounge").exists())
            .put("Wheelchair-accessible meeting spaces / business center", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible meeting spaces / business center").exists())
            .put("Wheelchair-accessible restaurant", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible restaurant").exists())
            .put("Wheelchair-accessible parking", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible parking").exists())
            .put("Wheelchair-accessible path of travel", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible path of travel").exists())
            .put("Wheelchair-accessible public washroom", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible public washroom").exists())
            .put("Wheelchair-accessible path to elevator", () -> propertyAmenitiesModal.getPropByText("Wheelchair-accessible path to elevator").exists())
            .put("Wheelchairs available on site", () -> propertyAmenitiesModal.getPropByText("Wheelchairs available on site").exists())
            .put("Braille signage", () -> propertyAmenitiesModal.getPropByText("Braille signage").exists())
            .put("Handrails in stairways", () -> propertyAmenitiesModal.getPropByText("Handrails in stairways").exists())
            .put("In-room accessibility available on request", () -> propertyAmenitiesModal.getPropByText("In-room accessibility").exists())
            .put("Roll-in shower available on request", () -> propertyAmenitiesModal.getPropByText("Roll-in shower").exists())
            .put("Accessible bathroom features available on request", () -> propertyAmenitiesModal.getPropByText("Accessible bathroom").exists())

            .put("Kid menu / kids meals", () -> propertyAmenitiesModal.getPropertyByTexts("children", "breakfast").exists()
                    || propertyAmenitiesModal.getPropertyByTexts("kids", "meal").exists())
            .put("Babysitting / onsite childcare", () -> propertyAmenitiesModal.getPropByText("Babysitting").exists())

            .put("Beach chairs/loungers", () -> propertyAmenitiesModal.getPropByText("Beach chairs").exists()
                    || propertyAmenitiesModal.getPropByText("Beach loungers").exists())
            .put("Beach umbrellas", () -> propertyAmenitiesModal.getPropByText("Beach umbrellas").exists())
            .put("Beach towels", () -> propertyAmenitiesModal.getPropByText("Beach towels").exists())
            .put("Fitness center", () -> propertyAmenitiesModal.getPropByText("Fitness center").exists())
            .put("Fitness facilities / fitness center rules", () -> propertyAmenitiesModal.getPropByText("Fitness facilities / fitness center rules").exists())
            .put("Fitness classes", () -> propertyAmenitiesModal.getPropByText("Fitness classes").exists())
            .put("Yoga classes", () -> propertyAmenitiesModal.getPropByText("Yoga classes").exists())
            .put("Pilates classes", () -> propertyAmenitiesModal.getPropByText("Pilates classes").exists())
            .put("Aerobics", () -> propertyAmenitiesModal.getPropByText("Aerobics").exists())
            .put("Golf", () -> propertyAmenitiesModal.getPropByText("Golf").exists())
            .put("Golf course", () -> propertyAmenitiesModal.getPropByText("Golf course").exists())
            .put("Outdoors areas", () -> propertyAmenitiesModal.getPropByText("Outdoors areas").exists())
            .put("Garden", () -> propertyAmenitiesModal.getPropByText("Garden").exists())
            .put("Picnic area", () -> propertyAmenitiesModal.getPropByText("Picnic area").exists())
            .put("Terrace", () -> propertyAmenitiesModal.getPropByText("Terrace").exists())
            .put("Kids' / Children\'s pool", () -> propertyAmenitiesModal.getPropByText("Children\'s pool").exists()
                    || propertyAmenitiesModal.getPropByText("Kid\'s pool").exists())
            .put("Poll umbrellas", () -> propertyAmenitiesModal.getPropByText("Poll umbrellas").exists())
            .put("Pool loungers or chairs", () -> propertyAmenitiesModal.getPropByText("Pool chairs").exists()
                    || propertyAmenitiesModal.getPropByText("Pool loungers").exists())
            .put("Lifeguards on site", () -> propertyAmenitiesModal.getPropByText("Lifeguards on site").exists())
            .put("Pool cabanas", () -> propertyAmenitiesModal.getPropByText("Pool cabanas").exists())
            .put("Pool towels", () -> propertyAmenitiesModal.getPropByText("Pool towels").exists())
            .put("Steam room in spa", () -> propertyAmenitiesModal.getPropertyByTexts("steam", "spa").exists())
            .put("Spa tub/jacuzzi in spa", () -> propertyAmenitiesModal.getPropByText("Spa tub").exists()
                    || propertyAmenitiesModal.getPropByText("jacuzzi in spa").exists())
            .put("Couple treatment rooms", () -> propertyAmenitiesModal.getPropByText("Couples treatment room").exists())
            //Smoking policy
            .put("Smoke-free property", () -> propertyAmenitiesModal.getPropByText("Smoke-free property").exists())
            .put("Smoking allowed on designated areas", () -> propertyAmenitiesModal.getPropByText("Smoking allowed on designated areas").exists())
            .put("Non-smoking Rooms available", () -> propertyAmenitiesModal.getPropByText("Non-smoking Rooms available").exists())
            .build();

    private Map<String, Callable<String>> amenitiesDescriptionMap = ImmutableMap.<String, Callable<String>>builder()
            .put("WIFI available in rooms", () -> propertyAmenitiesModal.getAmenitiesItemsTexts()
                    .stream().filter(sentence -> hasWords(sentence, Arrays.asList("wifi", "room", "all")))
                    .findFirst().orElse(StringUtils.EMPTY))
            .put("WIFI available in public areas", () -> propertyAmenitiesModal.getAmenitiesItemsTexts()
                    .stream().filter(sentence -> hasWords(sentence, Arrays.asList("wifi", "public", "areas")))
                    .findFirst().orElse(StringUtils.EMPTY))
            .put("Breakfast available", () -> propertyAmenitiesModal.getPropByText("breakfast").text())
            .put("Number of restaurants", () -> parseDigit(propertyAmenitiesModal.getPropByText("restaurants").text()))
            .put("Number of bars", () -> parseDigit(getSelenideElementText(propertyAmenitiesModal.getPropByText("bars"))))
            .put("Snack bar/deli", () -> parseDigitWithFollowingText(
                    getSelenideElementText(propertyAmenitiesModal.getPropByText("bars")), "beach bar"))
            .put("Room service", () -> propertyAmenitiesModal.getPropByText("room service").text())
            .put("Babysitting / onsite childcare",
                    () -> getSelenideElementText(propertyAmenitiesModal.getPropertyByTexts("child care")))
            .put("Kid\'s club",
                    () -> getSelenideElementText(propertyAmenitiesModal.getPropertyByTexts("children\'s club")))
            .put("Supervised childcare/activities",
                    () -> getSelenideElementText(propertyAmenitiesModal.getPropertyByTexts("supervised childcare/activities")))
            .put("Beach clubs", () -> getSelenideElementText(propertyAmenitiesModal.getPropByText("beach club")))
            .put("Beach bars", () -> MatcherUtils.parseDigitWithFollowingText(
                    getSelenideElementText(propertyAmenitiesModal.getPropByText("beach bar")),
                    "beach Bar"))
            .put("Outdoor pool", () -> MatcherUtils.parseDigitWithFollowingText(
                    getSelenideElementText(propertyAmenitiesModal
                            .getPropByText("outdoor pool")),
                    "outdoor pool"))
            .put("Treatment rooms",
                    () -> MatcherUtils.parseDigitWithFollowingText(getSelenideElementText(propertyAmenitiesModal
                            .getPropByText("treatment rooms")), "treatment rooms"))
            .put("Self Parking available",
                    () -> getSelenideElementText(propertyAmenitiesModal.getPropByText("Self parking")))
            .put("Rollaway/ extra beds available",
                    () -> getSelenideElementText(propertyAmenitiesModal.getPropByText("Rollaway")))
            .put("Cribs/infant beds available",
                    () -> getSelenideElementText(propertyAmenitiesModal.getPropByText("Free cribs/infant beds")))
            .build();


    public PropertyAmenitiesModalActions(ExpediaExelHelper expediaExelHelper) {
        this.expediaExelHelper = expediaExelHelper;

    }

    @Step("Populate Amenities Data")
    public void populateAmenitiesData() {
        amenitiesAvailabilityMap.forEach(this::checkPropertyAvailability);
        amenitiesDescriptionMap.forEach(this::checkPropertyDescription);
    }

    @Step("Verify \"{propertyName}\" available")
    private void checkPropertyDescription(String propertyName, Callable<String> stringCallable) {
        try {
            var description = stringCallable.call();
            if (!description.isEmpty()) {
                expediaExelHelper.addPropertyInfoAvailability(propertyName);
                expediaExelHelper.addPropertyInfoDescription(propertyName, description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Step("Verify \"{propertyName}\" available")
    private void checkPropertyAvailability(String propertyName, Callable<Boolean> booleanCallable) {
        try {
            if (booleanCallable.call()) {
                expediaExelHelper.addPropertyInfoAvailability(propertyName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getSelenideElementText(SelenideElement selenideElement) {
        if (!selenideElement.exists()) {
            return StringUtils.EMPTY;
        }
        return selenideElement.text();
    }
}
