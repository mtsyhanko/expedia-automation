package com.expedia.ui.actions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.expedia.exel.ExpediaExelHelper;
import com.expedia.ui.pages.HotelViewPage;
import com.expedia.ui.pages.RoomInfoModal;
import com.google.common.collect.ImmutableMap;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;

import static com.codeborne.selenide.Selenide.actions;

public class RoomInformationActions {
    private final ExpediaExelHelper expediaExelHelper;
    private HotelViewPage hotelViewPage;
    private RoomInfoModal roomInfoModal;
    private Map<String, Callable<Boolean>> roomAvailabilityMap = ImmutableMap.<String, Callable<Boolean>>builder()
            .put("Room type name", () -> roomInfoModal.getRoomInfoItemByText("Room type name").exists())
            .put("Single space/Milti-space", () -> roomInfoModal.getRoomInfoItemByText("Single space/Milti-space").exists())
            .put("Room size", () -> roomInfoModal.getRoomInfoItemByText("Room size").exists())
            .put("Views", () -> roomInfoModal.getRoomInfoItemByText("Views").exists())
            .put("Bedding options", () -> roomInfoModal.getRoomInfoItemByText("Bedding options").exists())
            .put("WIFI", () -> roomInfoModal.getRoomInfoItemByText("WIFI").exists())
            .put("Sitting area", () -> roomInfoModal.getRoomInfoItemByText("Sitting area ").exists())
            .put("Dinning area", () -> roomInfoModal.getRoomInfoItemByText("Dinning area ").exists())
            .put("Connecting rooms available", () -> roomInfoModal.getRoomInfoItemByText("Connecting rooms available").exists())
            .put("Smoking policy", () -> roomInfoModal.getRoomInfoItemByText("Smoking policy").exists())
            .put("No smoking room", () -> roomInfoModal.getRoomInfoItemByText("No smoking room").exists())
            .put("Room Amenities", () -> roomInfoModal.getRoomInfoItemByText("Room Amenities").exists())
            .put("Premium bedding", () -> roomInfoModal.getRoomInfoItemByText("Premium bedding").exists())
            .put("Hypo-allergenic bedding", () -> roomInfoModal.getRoomInfoItemByText("Hypo-allergenic bedding").exists())
            .put("Rollaway beds available", () -> roomInfoModal.getRoomInfoItemByText("Rollaway beds available").exists())
            .put("Safety deposit box", () -> roomInfoModal.getRoomInfoItemByText("Safety deposit box").exists())
            .put("Iron/ironing board", () -> roomInfoModal.getRoomInfoItemByText("Iron/ironing board").exists())
            .put("Blackout drapes/curtains", () -> roomInfoModal.getRoomInfoItemByText("Blackout drapes/curtains").exists())
            .put("Turndown service", () -> roomInfoModal.getRoomInfoItemByText("Turndown service").exists())
            .put("Air conditioning", () -> roomInfoModal.getRoomInfoItemByText("Air conditioning").exists())
            .put("Fan", () -> roomInfoModal.getRoomInfoItemByText("Fan").exists())
            .put("Desk", () -> roomInfoModal.getRoomInfoItemByText("Desk").exists())
            .put("Executive lounge access", () -> roomInfoModal.getRoomInfoItemByText("Executive lounge access ").exists())
            .put("Bathroom", () -> roomInfoModal.getRoomInfoItemByText("Bathroom").exists())
            .put("Bath configuration", () -> roomInfoModal.getRoomInfoItemByText("Bath configuration ").exists())
            .put("Toilet", () -> roomInfoModal.getRoomInfoItemByText("Toilet ").exists())
            .put("Bathrobes", () -> roomInfoModal.getRoomInfoItemByText("Bathrobes").exists())
            .put("Slippers", () -> roomInfoModal.getRoomInfoItemByText("Slippers").exists())
            .put("Free toiletries", () -> roomInfoModal.getRoomInfoItemByText("Free toiletries").exists())
            .put("Hair dryer", () -> roomInfoModal.getRoomInfoItemByText("Hair dryer").exists())
            .put("Towels", () -> roomInfoModal.getRoomInfoItemByText("Towels").exists())
            .put("Hand sanitizer", () -> roomInfoModal.getRoomInfoItemByText("Hand sanitizer").exists())
            .put("Jacuzzi / private pool", () -> roomInfoModal.getRoomInfoItemByText("Jacuzzi / private pool").exists())
            .put("Private spa tub / jacuzzi", () -> roomInfoModal.getRoomInfoItemByText("Private spa tub / jacuzzi ").exists())
            .put("Private pool", () -> roomInfoModal.getRoomInfoItemByText("Private pool ").exists())
            .put("Private plunge pool", () -> roomInfoModal.getRoomInfoItemByText("Private plunge pool ").exists())
            .put("Media & technology", () -> roomInfoModal.getRoomInfoItemByText("Media & technology").exists())
            .put("Satellite or cable channels", () -> roomInfoModal.getRoomInfoItemByText("Satellite or cable channels").exists())
            .put("Premium channels", () -> roomInfoModal.getRoomInfoItemByText("Premium channels").exists())
            .put("Flat-screen TV", () -> roomInfoModal.getRoomInfoItemByText("Flat-screen TV").exists())
            .put("Telephone", () -> roomInfoModal.getRoomInfoItemByText("Telephone").exists())
            .put("Alarm clock", () -> roomInfoModal.getRoomInfoItemByText("Alarm clock").exists())
            .put("Kitchen", () -> roomInfoModal.getRoomInfoItemByText("Kitchen ").exists())
            .put("Kitchenette", () -> roomInfoModal.getRoomInfoItemByText("Kitchenette ").exists())
            .put("Stovetop", () -> roomInfoModal.getRoomInfoItemByText("Stovetop").exists())
            .put("Refrigerator", () -> roomInfoModal.getRoomInfoItemByText("Refrigerator").exists())
            .put("Microwave", () -> roomInfoModal.getRoomInfoItemByText("Microwave").exists())
            .put("Dishwasher", () -> roomInfoModal.getRoomInfoItemByText("Dishwasher").exists())
            .put("Cookware, dishware, and utensils", () -> roomInfoModal.getRoomInfoItemByText("Cookware, dishware, and utensils").exists())
            .put("In-room food & drink", () -> roomInfoModal.getRoomInfoItemByText("In-room food & drink").exists())
            .put("Minibar", () -> roomInfoModal.getRoomInfoItemByText("Minibar").exists())
            .put("Coffee / Tea maker", () -> roomInfoModal.getRoomInfoItemByText("Coffee / Tea maker").exists())
            .put("Free bottled water", () -> roomInfoModal.getRoomInfoItemByText("Free bottled water").exists())
            .put("Room service", () -> roomInfoModal.getRoomInfoItemByText("Room service").exists())
            .put("In-room accessibility", () -> roomInfoModal.getRoomInfoItemByText("In-room accessibility").exists())
            .put("Entire unit wheelchair accessible", () -> roomInfoModal.getRoomInfoItemByText("Entire unit wheelchair accessible").exists())
            .put("Rate Plans", () -> roomInfoModal.getRoomInfoItemByText("Rate Plans").exists())
            .put("Parking", () -> roomInfoModal.getRoomInfoItemByText("Parking").exists())
            .build();

    private Map<String, Callable<String>> roomInfoDetailsMap = ImmutableMap.<String, Callable<String>>builder()
            .put("Room type name ", () -> roomInfoModal.getRoomTypeLabel().text())
            .put("Room size", () -> roomInfoModal.getRoomInfoItemByText("sq ft").text())
            .put("Views", () -> getSelenideElementTextIfExists(roomInfoModal.getRoomInfoItemByText("view")))
            .put("Bedding options", () -> roomInfoModal.getRoomInfoItemByText("Bed").text())
            .put("WIFI", () -> getSelenideElementTextIfExists(roomInfoModal.getRoomInfoItemByText("WiFi")))
            .put("Cribs/infant beds available",
                    () -> getSelenideElementTextIfExists(roomInfoModal.getRoomInfoItemByText("infant beds available")))
            .put("Rollaway beds available",
                    () -> getSelenideElementTextIfExists(roomInfoModal.getRoomInfoItemByText("Rollaway")))
            .put("Bath configuration",
                    () -> getSelenideElementTextIfExists(roomInfoModal.getRoomInfoItemByText("Shower")))
            .put("Refrigerator",
                    () -> getSelenideElementTextIfExists(roomInfoModal.getRoomInfoItemByText("fridge")))
            .put("Room service",
                    () -> getSelenideElementTextIfExists(roomInfoModal.getRoomInfoItemByText("room service")))

            .build();

    private Map<String, String> commonInfoMap = ImmutableMap.<String, String>builder()
            .put("Parking", "Self Parking available")
            .build();

    public RoomInformationActions(ExpediaExelHelper expediaExelHelper) {
        hotelViewPage = new HotelViewPage();
        roomInfoModal = new RoomInfoModal();
        this.expediaExelHelper = expediaExelHelper;
    }

    public List<String> getAvailableRoomsList() {
        hotelViewPage.getRoomsTab().click();
        hotelViewPage.getRoomNames().first().shouldBe(Condition.visible);
        return hotelViewPage.getRoomNames().texts();
    }

    @Step("Open '{room}' room modal")
    public void openRoomModal(String room) {
        hotelViewPage.getRoomsTab().scrollIntoView(true).click();
        hotelViewPage.getMoreDetailsButtonByName(room)
                .shouldBe(Condition.visible)
                .shouldBe(Condition.enabled)
                .scrollIntoView(false)
                .click();
        roomInfoModal.getModalHeader().shouldBe(Condition.visible);
    }

    @Step("Populate '{room}' data")
    public void populateRoomData(String room) {
        expediaExelHelper.addRoom(room);
        roomAvailabilityMap.forEach((key, value) -> {
            verifyRoomInfoAvailability(room, key, value);
        });

        roomInfoDetailsMap.forEach((key, value) -> {
            verifyRoomInfoDetails(room, key, value);
        });

        expediaExelHelper.populateGeneralRoomInfo(commonInfoMap);
    }

    @Step("Verify '{roomInfoItem}' available in room")
    private void verifyRoomInfoAvailability(String room, String roomInfoItem, Callable<Boolean> value) {
        try {
            if (value.call()) {
                expediaExelHelper.addRoomInfoAvailability(room, roomInfoItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Step("Verify '{roomInfoItem}' available in room")
    private void verifyRoomInfoDetails(String room, String roomInfoItem, Callable<String> value) {
        try {
            var description = value.call();
            if (!description.isEmpty()) {
                expediaExelHelper.addRoomInfoDescription(room, roomInfoItem, description);
                expediaExelHelper.addRoomInfoAvailability(room, roomInfoItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Step
    public void loadRoomImages(String room, String directoryPath) {
        List<String> imageSrcList = new ArrayList<>();
        var downloadDirectory = new File(directoryPath);
        if (downloadDirectory.exists()) {
            downloadDirectory.mkdirs();
        }

        do {
            var imageSrc = roomInfoModal.getImageButton().getAttribute("src");
            imageSrcList.add(imageSrc);
            try {
                var file = Selenide.download(Objects.requireNonNull(roomInfoModal.getImageButton().getAttribute("src")));
                var description = roomInfoModal.getImageButton().getAttribute("alt");
                FileUtils.copyFileToDirectory(file, downloadDirectory);
                expediaExelHelper.addRoomImage(room, new File(downloadDirectory + "/" + file.getName()).getAbsolutePath(), description);
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
            actions().pause(Duration.ofMillis(50))
                    .click(roomInfoModal.getButtonNext().shouldBe(Condition.visible))
                    .pause(Duration.ofMillis(100))
                    .perform();
            roomInfoModal.getImageButton().shouldNotBe(Condition.attribute("src", imageSrc));
        } while (!imageSrcList.contains(roomInfoModal.getImageButton().getAttribute("src")));
    }

    @Step
    public void closeRoomView() {
        roomInfoModal.getCloseButton().shouldBe(Condition.visible).click();
    }

    private String getSelenideElementTextIfExists(SelenideElement selenideElement) {
        if (!selenideElement.exists()) {
            return StringUtils.EMPTY;
        }
        return selenideElement.text();
    }
}
