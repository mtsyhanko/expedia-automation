package com.expedia.ui.actions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.ex.TimeoutException;
import com.expedia.exel.ExpediaExelHelper;
import com.expedia.ui.pages.*;
import com.google.common.collect.ImmutableMap;
import com.neovisionaries.i18n.CountryCode;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static com.expedia.ui.utils.MatcherUtils.*;
import static com.expedia.ui.utils.RandomWaitUtils.randomWait;

public class PropertyInformationActions {
    private HotelViewPage hotelViewPage;
    private ExpediaExelHelper expediaExelHelper;
    private PropertyAmenitiesModal propertyAmenitiesModal;
    private OverviewModal overviewModal;
    private PoliciesModal policiesModal;
    private GalleryViewModal galleryViewModal;

    private Map<String, Callable<Boolean>> generaInfoAvailabilityMap = ImmutableMap.<String, Callable<Boolean>>builder()
            .put("Ideal for business trips", () -> hotelViewPage.getBusinessServicesLabel().exists())
            .put("Express check-out", () -> policiesModal.getPolicyByText("Express check-out").exists())
            .put("Late check-out subject to availability", () -> policiesModal.getPolicyByText("Late check-in").exists())
            .put("Self / express check-in", () -> policiesModal.getPolicyByText("Self check-in").exists()
                    || policiesModal.getPolicyByText("Express check-in").exists())
            .put("Private check-in/check-out", () -> policiesModal.getPolicyByText("Private check-in/check-out").exists())
            .put("Pets not allowed", () -> policiesModal.getPolicyByText("Pets not allowed").exists())
            .put("Service animals welcomed", () -> policiesModal.getPolicyByText("Service animals welcomed").exists())
            .put("Children and extra beds policy", () -> policiesModal.getPolicyByText("Children and extra beds policy").exists())
            .put("Security System", () -> hotelViewPage.getImportantInfoSentences()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("security", "system", "include"))))
            .put("Smoke detectors / alarms", () -> hotelViewPage.getImportantInfoSentences()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("security", "detector", "include"),
                            Arrays.asList("security", "alarm", "include"))))
            .put("Follows industry or local sanitization guidelines", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("industry", "cleaning"))))
            .put("Contactless check-in/check-out", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("Contactless", "check-in", "check-out"))))
            .put("Protective shields in place at main contact areas", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("shields", "place", "main"))))
            .put("Social / physical distancing measures are in place", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("social", "distancing", "palace"),
                            Arrays.asList("physical", "distancing", "palace"))))
            .put("Cashless payment available", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("social", "distancing", "palace"),
                            Arrays.asList("physical", "distancing", "palace"))))
            .put("Face masks for guests available", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("masks", "available"),
                            Arrays.asList("masks", "free"))))
            .put("Hand sanitizer in guest accommodation and key areas", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("sanitizer", "available"),
                            Arrays.asList("sanitizer", "provided"))))
            .put("First aid kit available", () -> hotelViewPage.getImportantInfoSentences()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("first", "aid", "kit"))))
            .put("Personal protective equipment worn by staff", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("protective", "equipment", "staff"))))
            .put("Temperature checks given to staff", () -> hotelViewPage.getCleaningAndSafetyPractices()
                    .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("temperature", "checks", "staff"))))
            .put("Individually-wrapped food options available and delivered food is covered securely",
                    () -> hotelViewPage.getCleaningAndSafetyPractices()
                            .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("Individually-wrapped", "food", "options"))))
            .put("Contactless room service",
                    () -> hotelViewPage.getCleaningAndSafetyPractices()
                            .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("contactless", "room service"))))
            .put("Enhanced food service safety measures",
                    () -> hotelViewPage.getCleaningAndSafetyPractices()
                            .stream().anyMatch(sentence -> hasWords(sentence, Arrays.asList("food service", "safety measures"))))
            .build();

    private Map<String, Callable<String>> generaInfoDetailsMap = ImmutableMap.<String, Callable<String>>builder()
            .put("Check In", () -> policiesModal.getCheckInInfoLabel().text())
            .put("Check Out", () -> policiesModal.getCheckOutInfoLabel().text())
            .put("Total Available Rooms", () -> parseDigitWithFollowingText(hotelViewPage.getPropertyDescriptionSection().text(), "rooms"))
            .put("Address 1", () -> hotelViewPage.getAddress1MetaElement().getAttribute("content"))
            .put("Country",
                    () -> Objects.requireNonNull(
                            CountryCode.getByCode(hotelViewPage.getAddressCountryCode().getAttribute("content"))).getName())
            .put("State / Province", () -> hotelViewPage.getAddressRegionCode().text())
            .put("City", () -> hotelViewPage.getAddressCity().getAttribute("content"))
            .put("Nearest Airport", () -> hotelViewPage.getAirportLabel().text())
            .build();

    public PropertyInformationActions(ExpediaExelHelper expediaExelHelper) {
        hotelViewPage = new HotelViewPage();
        propertyAmenitiesModal = new PropertyAmenitiesModal();
        galleryViewModal = new GalleryViewModal();
        policiesModal = new PoliciesModal();
        overviewModal = new OverviewModal();
        this.expediaExelHelper = expediaExelHelper;
    }

    public void openPopularAmenities() {
        randomWait();
        hotelViewPage.getSeeAllPopularAmenities().shouldBe(Condition.visible).click();
        propertyAmenitiesModal.getCloseModalViewButton().shouldBe(Condition.visible);
    }

    @Step
    public void loadImages(String directoryPath) {
        randomWait();
        hotelViewPage.getGalleryButtons().first().click();
        galleryViewModal.getButtonNext().shouldBe(Condition.visible);
        List<String> imageCounts;
        var downloadDirectory = new File(directoryPath);
        if (downloadDirectory.exists()) {
            downloadDirectory.mkdirs();
        }

        do {
            try {
                var file = Selenide.download(Objects.requireNonNull(galleryViewModal.getImage()
                        .getAttribute("src")));
                var imageDescription = galleryViewModal.getImage().getAttribute("alt");
                FileUtils.copyFileToDirectory(file, downloadDirectory);
                expediaExelHelper.addGeneralImage(new File(downloadDirectory
                        + "/" + file.getName()).getAbsolutePath(), imageDescription);

            } catch (IOException | URISyntaxException | TimeoutException e) {
                e.printStackTrace();
            }
            randomWait();
            galleryViewModal.getButtonNext().shouldBe(Condition.visible).click();
            imageCounts = Arrays.asList(galleryViewModal.getImageCounter().getText().split("/")).stream()
                    .map(String::trim).collect(Collectors.toList());
        } while (!imageCounts.get(0).equals(imageCounts.get(1)));
        galleryViewModal.getCloseButton().shouldBe(Condition.visible).click();

    }

    @Step("Populate Hotel General Info")
    public void populateGeneralInfo() {
        generaInfoAvailabilityMap.forEach(this::checkPropertyAvailability);
        generaInfoDetailsMap.forEach(this::checkPropertyDescription);
        populateListOfRestaurants();
        populateListOfRestaurants();
    }

    @Step("Verify \"{propertyName}\" available")
    private void checkPropertyDescription(String propertyName, Callable<String> stringCallable) {
        try {
            var description = stringCallable.call();
            if (!description.isEmpty()) {
                expediaExelHelper.addPropertyInfoAvailability(propertyName);
                expediaExelHelper.addPropertyInfoDescription(propertyName, description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Step("Verify \"{propertyName}\" available")
    private void checkPropertyAvailability(String propertyName, Callable<Boolean> booleanCallable) {
        try {
            if (booleanCallable.call()) {
                expediaExelHelper.addPropertyInfoAvailability(propertyName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void populateListOfRestaurants() {
        var restaurantsOnSite = overviewModal.getRestaurantsList().texts()
                .stream()
                .filter(restaurantInfo -> Integer.parseInt(parseDigit(restaurantInfo)) < 2)
                .map(restaurantInfo -> restaurantInfo.split("—")[NumberUtils.INTEGER_ZERO])
                .collect(Collectors.toList());

        expediaExelHelper.addPropertyItemAfterElement("List of restaurants on-site", restaurantsOnSite);
    }
}
