package com.expedia.exel;

import com.expedia.ui.models.ItemModel;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ExpediaExelHelper {
    private final static String PROPERTY_INFO = "Property info";
    private final static String PROPERTY_INFO_TEMPLATE_PATH = "src/test/resources/template/propertyinfo_template.csv";
    private static final String ROOM_INFO_TEMPLATE_PATH = "src/test/resources/template/roominfo_template.csv";
    private static final int IMAGES_COLUMN_INDEX = 3;
    private ExelHelper exelHelper;
    private List<ItemModel> itemPropertyInfoModels = new ArrayList<>();
    private List<Pair<String, String>> generalImagesList = new ArrayList<>();
    private Map<String, List<Pair<String, String>>> roomsImagesList = new HashMap<>();
    private Map<String, List<ItemModel>> itemRoomsList = new HashMap<>();

    public ExpediaExelHelper(String directory, String fileName) {
        exelHelper = new ExelHelper();
        exelHelper.createExelFile(directory, fileName);
        exelHelper.addSheet(PROPERTY_INFO);
        exelHelper.addHeader(PROPERTY_INFO, "Available", 0, 10);
        exelHelper.addHeader(PROPERTY_INFO, "Content item name", 1, 90);
        exelHelper.addHeader(PROPERTY_INFO, "Content item description", 2, 30);
        exelHelper.addHeader(PROPERTY_INFO, "Images", 3, 60);

        List<String> itemNames = null;

        try {
            CSVReader csvReader
                    = new CSVReader(new FileReader(PROPERTY_INFO_TEMPLATE_PATH));
            itemNames = csvReader.readAll()
                    .stream().map(arr -> arr[NumberUtils.INTEGER_ZERO]).collect(Collectors.toList());
        } catch (CsvException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        itemNames.forEach(value -> {
            var itemModel = ItemModel.builder().itemName(value).build();
            itemPropertyInfoModels.add(itemModel);
        });
    }

    public void addPropertyInfoAvailability(String itemName) {
        itemPropertyInfoModels.stream().filter(itemModel -> itemModel.getItemName().equals(itemName))
                .forEach(itemModel -> itemModel.setItemAvailability(true));
    }

    public void addRoomInfoAvailability(String room, String itemName) {
        itemRoomsList.get(room).stream().filter(itemModel -> itemModel.getItemName().equals(itemName))
                .forEach(itemModel -> itemModel.setItemAvailability(true));
    }

    public void addPropertyInfoDescription(String itemName, String description) {
        itemPropertyInfoModels.stream().filter(itemModel -> itemModel.getItemName().equals(itemName))
                .forEach(itemModel -> itemModel.setItemDescription(description));
    }

    public void addRoomInfoDescription(String room, String itemName, String description) {
        itemRoomsList.get(room).stream().filter(itemModel -> itemModel.getItemName().equals(itemName))
                .forEach(itemModel -> itemModel.setItemDescription(description));
    }

    public void addRoom(String room) {
        exelHelper.addSheet(room);
        exelHelper.addHeader(room, "Available", 0, 8);
        exelHelper.addHeader(room, "Content item name", 1, 100);
        exelHelper.addHeader(room, "Content item description", 2, 30);
        exelHelper.addHeader(room, "Images", 3, 30);

        List<String> itemNames = null;
        itemRoomsList.put(room, new ArrayList<>());

        try {
            CSVReader csvReader
                    = new CSVReader(new FileReader(ROOM_INFO_TEMPLATE_PATH));
            itemNames = csvReader.readAll().stream().map(arr -> arr[NumberUtils.INTEGER_ZERO]).collect(Collectors.toList());
        } catch (CsvException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        itemNames.forEach(value -> {
            var itemModel = ItemModel.builder().itemName(value).build();
            itemRoomsList.get(room).add(itemModel);
        });
    }

    public void save() {
        IntStream.range(1, itemPropertyInfoModels.size()).forEach(index -> {
            var itemMode = itemPropertyInfoModels.get(index - 1);
            exelHelper.addCellValue(PROPERTY_INFO, itemMode.getItemName(), index, 1);
            exelHelper.addCellValue(PROPERTY_INFO, itemMode.isItemAvailability() ? "x" : "", index, 0);
            exelHelper.addCellValue(PROPERTY_INFO, itemMode.getItemDescription(), index, 2);
        });

        itemRoomsList.forEach((key, value) -> {
            IntStream.range(1, value.size()).forEach(index -> {
                var itemMode = value.get(index - 1);
                exelHelper.addCellValue(key, itemMode.getItemName(), index, 1);
                exelHelper.addCellValue(key, itemMode.isItemAvailability() ? "x" : "", index, 0);
                exelHelper.addCellValue(key, itemMode.getItemDescription(), index, 2);
            });
        });

        exelHelper.addHyperLinks(PROPERTY_INFO, generalImagesList, IMAGES_COLUMN_INDEX);
        roomsImagesList.forEach((room, imagesList) -> {
            exelHelper.addHyperLinks(room, imagesList, IMAGES_COLUMN_INDEX);
        });

        exelHelper.write();
    }

    public void addPropertyItemAfterElement(String columnName, List<String> items) {
        var index = itemPropertyInfoModels.indexOf(itemPropertyInfoModels.stream()
                .filter(item -> item.getItemName().equals(columnName)).findFirst().get());
        items.forEach(item -> {
            var itemModel = ItemModel.builder().itemAvailability(true).itemName("Name")
                    .itemDescription(item).build();
            itemPropertyInfoModels.add(index + NumberUtils.INTEGER_ONE, itemModel);
        });
    }

    public void populateGeneralRoomInfo(Map<String, String> commonInfoMap) {
        itemRoomsList.forEach((room, model) -> {
            commonInfoMap.forEach((key, value) -> {
                var generalItem = itemPropertyInfoModels.stream()
                        .filter(item -> item.getItemName().equals(value)).findFirst().get();
                if (generalItem.isItemAvailability()) {
                    addRoomInfoAvailability(room, key);
                }
            });
        });
    }

    public void addGeneralImage(String absolutePath, String description) {
        generalImagesList.add(Pair.of(absolutePath, description));
    }

    public void addRoomImage(String room, String path, String description) {
        if (!roomsImagesList.containsKey(room)) {
            roomsImagesList.put(room, new ArrayList<>());
        }
        roomsImagesList.get(room).add(Pair.of(path, description));
    }
}
