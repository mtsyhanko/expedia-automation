package com.expedia.exel;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.*;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class ExelHelper {
    public static Logger logger = Logger.getLogger(ExelHelper.class);
    private WritableWorkbook workbook;
    private Map<String, WritableSheet> writableSheetMap = new HashMap<>();

    public void createExelFile(String directoryName, String fileName) {
        var directory = new File(directoryName);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        try {
            workbook = Workbook.createWorkbook(new File(directory + "/" + fileName));
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void addSheet(String sheetName) {
        WritableSheet sheet = workbook.createSheet(sheetName, writableSheetMap.size());
        writableSheetMap.put(sheetName, sheet);
    }

    public void addHeader(String sheetName, String sheetHeaderValue, int sheetHeaderIndex, int headerSize) {
        try {
            WritableCellFormat headerFormat = new WritableCellFormat();
            WritableFont font
                    = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD);
            headerFormat.setFont(font);
            headerFormat.setBackground(Colour.WHITE);
            headerFormat.setWrap(true);
            headerFormat.setAlignment(Alignment.LEFT);

            Label headerLabel = new Label(sheetHeaderIndex, 0, sheetHeaderValue, headerFormat);
            writableSheetMap.get(sheetName).setColumnView(sheetHeaderIndex, headerSize);
            writableSheetMap.get(sheetName).addCell(headerLabel);

        } catch (WriteException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void addColumnValues(String sheetName, List<String> values, int columnIndex) {
        IntStream
                .range(1, values.size())
                .forEach(index -> addCellValue(sheetName, values.get(index - 1), index, columnIndex));
    }

    public void addCellValue(String sheetName, String value, int rowIndex, int columnIndex) {
        try {
            WritableCellFormat cellFormat = new WritableCellFormat();
            cellFormat.setWrap(true);

            writableSheetMap.get(sheetName)
                    .addCell(new Label(columnIndex, rowIndex, value, cellFormat));
        } catch (WriteException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void addHyperLinks(String sheetName, List<Pair<String, String>> values, int columnIndex) {
        IntStream
                .range(1, values.size())
                .forEach(index -> addHyperLink(sheetName, values.get(index - 1).getLeft(),
                        values.get(index - 1).getRight(), index, columnIndex));
    }

    public void addHyperLink(String sheetName, String value, String description, int rowIndex, int columnIndex) {
        try {
            writableSheetMap.get(sheetName)
                    .addHyperlink(new WritableHyperlink(columnIndex, rowIndex, new File(value), description));

        } catch (WriteException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void write() {
        try {
            workbook.write();
            workbook.close();
        } catch (IOException | WriteException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
